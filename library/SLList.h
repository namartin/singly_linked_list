#ifndef MYLIST_H
#define MYLIST_H

#include <stdio.h>
#include <stdlib.h>


class SLList 
{
public:
    struct node
    {
      int32_t data;
      node* ptr;
      
      ~node();
    };
    
private:
    node* head;
    node* tail;
    int32_t size;

public:
    /// Constructor
    SLList();
    
    /// Copy constructor
    SLList(const SLList& orig);
    
    /// Destructor
    virtual ~SLList();
//    ~MyList();
    
    /// Append a node to the end of the list
    /// 
    /// \param value         Value of the node's data
    ///
    /// \return 0 if successful; else nonzero
    int32_t appendNode(int32_t value);
    
    /// Insert a node at the given index
    /// 
    /// \param index         Index where to insert the node
    /// \param value         Value for the node's data
    ///
    /// \return 0 if successful; else nonzero
    int32_t insertNode(int32_t index, int32_t value);
    
    /// Delete node at the index
    /// 
    /// \param index         Index of the node to be deleted
    ///
    /// \return 0 if successful; else nonzero
    int32_t deleteNode(int32_t index);
    
    /// Return the total number of nodes
    /// 
    /// \return 
    int32_t getSize() const;
    
    /// Print list to console
    void displayList() const;
    
    node* findHead() const;
    
    node* findTail() const;
    
    /// Get node at a given index
    /// 
    /// \param index         Index of the node you want
    ///
    /// \return a pointer to the node at the given index
    node* getNode(int32_t index) const;
};

#endif /* MYLIST_H */