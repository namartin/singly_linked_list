#include <cstdlib>
#include "library/SLList.h"

using namespace std;


int main(int argc, char** argv)
{
    SLList* list;
    
    SLList::node* head;
    SLList::node* tail;
    int32_t size;
    int32_t append_retval;
    int32_t insert_retval;
    int32_t delete_retval;
    SLList::node* node;
    
    
    list = new SLList();
    
    // empty list
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    delete_retval = list->deleteNode(0); // empty list so no changes made; delete_retval = -1
    list->displayList(); // "Empty list\n"
    
    // append temporary node at end (index 0)
    append_retval = list->appendNode(3); // should be 0
    head = list->findHead(); // data = 3
    tail = list->findTail(); // data = 3
    size = list->getSize(); // should be 1
    list->displayList(); // 3
    
    // delete temporary node (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // append node at end (index 0)
    append_retval = list->appendNode(10); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 10
    size = list->getSize(); // should be 1
    list->displayList(); // 10
    
    // append node at end (index 1)
    append_retval = list->appendNode(20); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 20
    size = list->getSize(); // should be 2
    list->displayList(); // 10 20
    
    // append node at end (index 2)
    append_retval = list->appendNode(30); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 20 30
    
     // insert nodes at invalid indices -- no changes made; retval = -1
    insert_retval = list->insertNode(-5, 50); // should be -1
    insert_retval = list->insertNode(10, 60); // should be -1
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 20 30
    
    // insert node at index 0
    insert_retval = list->insertNode(0, 5); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 4
    list->displayList(); // 5 10 20 30
    
    // insert node at index 2
    insert_retval = list->insertNode(2, 15); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 5
    list->displayList(); // 5 10 15 20 30
    
    // insert node at index 5 (end)
    insert_retval = list->insertNode(5, 40); // should be 0
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 6
    list->displayList(); // 5 10 15 20 30 40
    
    // test the getNode() method
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(6); // index too high; node = NULL
    node = list->getNode(0); // data = 5
    node = list->getNode(1); // data = 10
    node = list->getNode(2); // data = 15
    node = list->getNode(-1); // index too low; node = NULL
    node = list->getNode(3); // data = 20
    node = list->getNode(4); // data = 30
    node = list->getNode(5); // data = 40
    node = list->getNode(10); // index too high; node = NULL
    
    // delete nodes at invalid indices -- no changes made; retval = -1
    delete_retval = list->deleteNode(-5); // should be -1
    delete_retval = list->deleteNode(6); // should be -1
    head = list->findHead(); // data = 5
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 6
    list->displayList(); // 5 10 15 20 30 40
    
    // delete node at index 0
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 5
    list->displayList(); // 10 15 20 30 40
    
    // delete node at index 2
    delete_retval = list->deleteNode(2); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 40
    size = list->getSize(); // should be 4
    list->displayList(); // 10 15 30 40
    
    // delete node at end (index 3)
    delete_retval = list->deleteNode(3); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 3
    list->displayList(); // 10 15 30
    
    // delete node at index 1
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 30
    size = list->getSize(); // should be 2
    list->displayList(); // 10 30
    
    // delete node at end (index 1)
    delete_retval = list->deleteNode(1); // should be 0
    head = list->findHead(); // data = 10
    tail = list->findTail(); // data = 10
    size = list->getSize(); // should be 1
    list->displayList(); // 10
    
    // delete node at beginning (index 0)
    delete_retval = list->deleteNode(0); // should be 0
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    // delete nodes from empty list -- no changes made; retval = -1
    delete_retval = list->deleteNode(0); // should be -1
    head = list->findHead(); // should point to 0
    tail = list->findTail(); // should point to 0
    size = list->getSize(); // should be 0
    list->displayList(); // "Empty list\n"
    
    
    list->appendNode(5);
    list->appendNode(6);
    list->appendNode(7);
    list->appendNode(8);
    delete list;
	list = 0;
    
    return 0;
}
